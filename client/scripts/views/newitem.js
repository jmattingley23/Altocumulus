if(Meteor.isClient) {
  Template.newitem.events({
    'click #newitem': function(e, t) {
      e.preventDefault();
      var title = t.find('#titleBox').value;
      var name = t.find('#nameBox').value;
      var info = t.find('#infoBox').value;

      if(title && name) { //both were filled in
        Meteor.call('addItem', title, name, info);
        Meteor._reload.reload();
      }
    }
  });
}
