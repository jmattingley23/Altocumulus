if(Meteor.isClient) {
  Template.itemdetail.helpers({
    title: function() {
      return Items.findOne(
        {'_id': Router.current().params.item}).title;
    },
    addedby: function() {
      return Items.findOne(
        {'_id': Router.current().params.item}).addedby;
    },
    date: function() {
      return Items.findOne(
        {'_id': Router.current().params.item}).date;
    },
    moreinfo: function() {
      return Items.findOne(
        {'_id': Router.current().params.item}).info;
    }
  });
}
