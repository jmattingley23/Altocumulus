if(Meteor.isClient) {
  Items = new Meteor.Collection('items');
  Meteor.subscribe('listData');

  Template.list.helpers({
    items: function() {
      return Items.find({'active': true}, {sort: {date: -1}});
    },
    disitems: function() {
      return Items.find({'active': false}, {sort: {date: 1}});
    }
  });

  Template.item.helpers({
    hasMoreInfo: function() {
      var id = Template.currentData()._id;
      var result = Items.findOne(
        {'_id': id}
      );
      if(result.info) {
        return "visible";
      }
      return "hidden";
    }
  });
  Template.item.events({
    'click #moreInfoButton': function(event, template) {
      event.preventDefault();
      Router.go('/itemdetail/' + template.find('.itemId').innerHTML);
    },
    'click .checkbutton': function(event, template) {
      event.preventDefault();

      Items.update(
        {'_id': template.find('.itemId').innerHTML},
        {$set: {'active': false}});
    }
  });

  Template.disitem.helpers({
    hasMoreInfo: function() {
      var id = Template.currentData()._id;
      var result = Items.findOne(
        {'_id': id}
      );
      if(result.info) {
        return "visible";
      }
      return "hidden";
    }
  });
  Template.disitem.events({
    'click #moreInfoButton': function(event, template) {
      event.preventDefault();
      Router.go('/itemdetail/' + template.find('.itemId').innerHTML);
    },
    'click .checkbutton': function(event, template) {
      event.preventDefault();

      Items.update(
        {'_id': template.find('.itemId').innerHTML},
        {$set: {'active': true}});
    },
    'click #deleteButton': function(event, template) {
      event.preventDefault();

      Items.remove(
        {'_id': template.find('.itemId').innerHTML}
      );
    }
  });
}
