if(Meteor.isServer) {
  Meteor.startup(function () {
    Meteor.publish('listData', function () {
      return Items.find();
    });
    Meteor.methods({
      /*
      * Adds a new item to the database with the given info
      */
      'addItem': function(title, person, info) {
        Items.insert({
          'title': title,
          'addedby': person,
          'date': moment(new Date()).format("dddd, MMM Do, YYYY"), //Monday, Jan 1st, 2017
          'info': info,
          'active': true
        });
        console.log('item added');
      }
    });
  });

  Router.route('/additem', {where: 'server'})
    .post(function() {
      console.log('post received');
      console.log(this.request.body);
      Meteor.call('addItem', this.request.body.title, this.request.body.name, this.request.body.info);
      this.response.setHeader('Content-Type', 'application/json');
      this.response.end(JSON.stringify('test'));
  });
}
